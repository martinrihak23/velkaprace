from django.db import models
from django.contrib.auth.models import User

class Employee(models.Model):
    username = models.CharField(max_length=30)
    name = models.CharField(max_length=30)
    surname = models.CharField(max_length=30)
    email = models.CharField(max_length=30 ,blank=True,null=True)
    password = models.CharField(max_length=30)
    tel_number = models.IntegerField(blank=True,null=True)
    role = models.CharField(max_length=10 ,blank=True,null=True)
    
    def __str__(self):
        return "{} {} - {}".format(self.name, self.surname, self.role)
class Customer(models.Model):
    username = models.CharField(max_length=30)
    password = models.CharField(max_length=30)
    name = models.CharField(max_length=30)
    surname = models.CharField(max_length=30)
    email = models.CharField(max_length=30,blank=True,null=True)
    tel_number = models.IntegerField(blank=True,null=True)
    
    def __str__(self):
        return "{} {}".format(self.name, self.surname)
class Wine(models.Model):
    name = models.CharField(max_length=50)
    barva = models.CharField(max_length=10)
    privlastek = models.CharField(max_length=20)
    rocnik = models.CharField(max_length=4)
    vyrPocet  = models.IntegerField()
    aktPocet = models.IntegerField()
    Cena = models.IntegerField()
    created = models.DateTimeField(auto_now_add= True)
    creator = models.ForeignKey(Employee,on_delete=models.SET_NULL, null=True)

    def __str__(self):
        return "{} {}".format(self.name,self.rocnik)
    
    
class Objednavka(models.Model):
    customer = models.ForeignKey(Customer, on_delete=models.SET_NULL, null=True)
    celkova_cena = models.IntegerField(default=0)
    pocet_prodejitem = models.IntegerField(default=0)
    zpracovana = models.BooleanField(default=False)
    casTrzby = models.DateTimeField(auto_now_add=True)

class Polozka(models.Model):
    prodej = models.ForeignKey(Objednavka, related_name='items', on_delete=models.CASCADE)
    wine = models.ForeignKey(Wine, on_delete=models.CASCADE)
    mnozstvi = models.IntegerField()
    
class Comment(models.Model):
    user = models.ForeignKey(Customer,on_delete=models.CASCADE)
    wine = models.ForeignKey(Wine,on_delete=models.CASCADE)
    body = models.TextField()
    update = models.DateTimeField(auto_now=True)
    created = models.DateTimeField(auto_now_add= True)

    def __str__(self):
        return self.body[0:50]

