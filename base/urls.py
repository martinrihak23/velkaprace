from django.urls import path
from . import views

urlpatterns = [
    path('login/',views.login,name="login"),
    path('logout/',views.logout, name="logout"),

    path('',views.login,name="login_blank"),
    path('home/',views.home, name='home'),
    path('creator/',views.creator,name='creator'),
    path('InfoOdrudy/',views.infoOdrudy,name='infoOdrudy'),
#-------------Chyby-------------------------------
    

#-------------Wine----------------------------------
    path("wine/",views.wine,name="wine"),
    path('add_wine/',views.add_wine,name='add_wine'),
    path('edit_wine/<str:pk>/',views.edit_wine,name='edit_wine'),
    path('delete_win/<str:pk>/',views.delete_wine,name='delete_wine'),

#---------Prodej-------------------------------
    path("objednavka/",views.objednavka, name="objednavka"),
    path("add_objednavka/",views.add_objednavka,name="add_objednavka"),
    path("pridatPolozku/<int:objednavka_id>/",views.pridatPolozku,name="pridat_polozku"),
    path("view_objednavka/<int:objednavka_id>/",views.viewObjednavka,name='view_objednavka'),
    path('moreWines/<int:wine_id>/',views.moreWines,name='moreWines'),
    path('update_objednavky/', views.update_objednavky, name='update_objednavky'),

#-------Emloyeee--------------------------------
    path("employee/",views.employee,name='employee'),
    path("customer/",views.customer,name='customer'),
    path("comments/<int:wine_id>/",views.comment,name='comments'),
    path("create_comment/<int:wine_id>/",views.add_comment,name='create_comment'),
    

]