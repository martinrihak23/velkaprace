from django.shortcuts import render,redirect
from .models import Wine,Objednavka, Employee, Customer,Comment,Polozka
from .forms import WineForm, ObjForm, PolozkaForm,CommentForm,LoginForm
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.urls import reverse_lazy,reverse
from django.forms import inlineformset_factory
from django.db.models import Sum,F
from django.core.exceptions import ValidationError
from django.contrib.auth.models import User
from django.contrib import messages
import django.http

# -------------Login---------------------------------
def logout(request):
    try:
        del request.session['logged']
        del request.session['login']
    except:
        return render(request, 'base/logout.html', {})
    return render(request, 'base/logout.html', {})

def login(request):
    if request.method == 'POST':
        form =  LoginForm(request.POST)
        if form.is_valid():
            l = form.cleaned_data['login_username']
            p = form.cleaned_data['login_password']
            try:
                user = Employee.objects.filter(username=l)[0]
                if user.password == p:
                    request.session['logged'] = True
                    request.session['login'] = user.username
                    return django.http.HttpResponseRedirect('base/home.html')
                else:
                    print('[LOGIN] Wrong password')
                    return render(request, 'base/login.html', {'form': form, 'login_message': 'Bad login'})
            except:
                try:
                    user = Customer.objects.filter(username=l)[0]
                    if user.password == p:
                        request.session['logged'] = True
                        request.session['login'] = user.username
                        return django.http.HttpResponseRedirect('base/home.html')
                    else:
                        print('[LOGIN] Wrong password')
                        return render(request, 'base/login.html', {'form': form, 'login_message': 'Bad login'})
                except:
                    print('[LOGIN] Non-existing user')
                    return render(request, 'base/login.html', {'form': form, 'login_message': 'Bad login'})
        else:
            print('[LOGIN] Invalid form passed')
            return render(request, 'base/login.html', {'form': form, 'login_message': 'Bad login'})
    else:
        form = LoginForm()
    return render (request,'base/login.html', {'form': form, 'login_message': ''})



def home(request):
    login_name = request.session['login']
    context = {'name':login_name}
    return render(request,'base/home.html',context)

def infoOdrudy(request):
    return render(request,'base/infoOdrudy.html')

def creator(request):
    context = {}
    return render(request,'base/creator.html',context)
#----------WIne -----------------------------------------------------
def wine(request):
    wines = Wine.objects.all()
    return render(request, 'base/wine.html', {'wines': wines})

def add_wine(request):
    form = WineForm()
    if request.method == 'POST':
        form = WineForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('wine')
    context = {'form':form}
    return render(request,'base/add_wine.html',context)

def edit_wine(request,pk):
    wine = Wine.objects.get(id=pk)
    form = WineForm(instance=wine)

    if request.method == 'POST':
        form =WineForm(request.POST,instance=wine)
        if form.is_valid():
            form.save()
            return redirect('wine')
    context = {'form':form}
    return render(request,'base/add_wine.html',context)

def delete_wine(request,pk):
    wine = Wine.objects.get(id=pk)
    if request.method == 'POST':
        wine.delete()
        return redirect('wine')
    return render(request,'base/delete.html',{'obj':wine})

#---------Prodej
def objednavka(request):
    obj = Objednavka.objects.all()
    context = {'obj':obj}
    return render(request,'base/objednavka.html',context)

def add_objednavka(request):
    form = ObjForm()
    if request.method == 'POST':
        form = ObjForm(request.POST)
        if form.is_valid():
            objednavka = form.save()  # Uložení nové objednávky do proměnné
            # Přesměrování na stránku pro přidání položek s ID nově vytvořené objednávky
            return redirect(reverse('pridat_polozku', kwargs={'objednavka_id': objednavka.id}))
    context = {'form': form}
    return render(request, 'base/add_obj.html', context)

from django.db.models import F

def pridatPolozku(request, objednavka_id):
    objednavka = Objednavka.objects.get(id=objednavka_id)  # Získání objednávky podle ID
    form = PolozkaForm()
    if request.method == 'POST':
        form = PolozkaForm(request.POST)
        if form.is_valid():
            polozka = form.save(commit=False)
            polozka.prodej = objednavka  # Přiřazení objednávky k položce
            
            # Kontrola aktuálního počtu daného vína v polozce
            aktPocet_vina = polozka.wine.aktPocet
            mnozstvi = polozka.mnozstvi
            if aktPocet_vina < mnozstvi:
                raise ValidationError("Nedostatečný počet vín na skladu.")
            
            # Aktualizace počtu položek v objednávce
            objednavka.pocet_prodejitem = objednavka.items.count() + 1
            
            # Výpočet celkové ceny objednávky
            cena_vina = polozka.wine.Cena
            objednavka.celkova_cena = F('celkova_cena') + cena_vina * mnozstvi
            objednavka.save()
            
            # Aktualizace aktuálního počtu daného vína v polozce
            polozka.wine.aktPocet = F('aktPocet') - mnozstvi
            polozka.save()
            
            return redirect(reverse('pridat_polozku', kwargs={'objednavka_id': objednavka_id}))  # Přesměrování na stejnou stránku pro přidání další položky
    context = {'polo': form, 'objednavka': objednavka}
    return render(request, 'base/pridat_polozku.html', context)

def moreWines(request,wine_id):
    wine = Wine.objects.get(id=wine_id)
    context = {'wine':wine}
    return render(request,'base/chyby/moreWines.html',context)

def update_objednavky(request):
    if request.method == 'POST':
        objednavky = Objednavka.objects.all()
        for objednavka in objednavky:
            checkbox_name = f'zpracovana_{objednavka.id}'
            if checkbox_name in request.POST:
                objednavka.zpracovana = True
            else:
                objednavka.zpracovana = False
            objednavka.save()
    return redirect('objednavka') 

def viewObjednavka(request,objednavka_id):
    objednavka = Objednavka.objects.get(id=objednavka_id)  # Získání objednávky podle ID
    context = {'objednavka':objednavka}
    return render(request,'base/view_objednavka.html',context)


def employee(request):
    employees = Employee.objects.all()
    context = {'employees':employees}
    return render(request,'base/employee.html',context)


def customer(request):
    customers = Customer.objects.all()
    context = {'customers':customers}
    return render(request,'base/customer.html',context)


def comment(request,wine_id):
    wine = Wine.objects.get(id=wine_id)
    commets = Comment.objects.filter(wine=wine)
    context = {'wine':wine,'comments':commets}
    return render(request,'base/comments.html',context)

def add_comment(request,wine_id):
    wine = Wine.objects.get(id=wine_id)
    form = CommentForm()
    customer = Customer.objects.all()
    if request.method == 'POST':
        form = CommentForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('comments',wine_id=wine_id)
    context = {'form':form,'users':customer,'wine':wine}
    return render(request,'base/add_comment.html',context)