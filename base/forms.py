from django.forms import ModelForm, DateTimeInput,HiddenInput
from django import forms
from .models import Wine,Objednavka,Polozka,Comment
class LoginForm(forms.Form):
    login_email = forms.CharField(label='login_username', max_length=30)
    login_password = forms.CharField(label='login_password', max_length=30)

class WineForm(ModelForm):
    class Meta:
        model = Wine
        fields = '__all__'

class ObjForm(ModelForm):
    class Meta:
        model = Objednavka
        exclude = ['celkova_cena','pocet_prodejitem']

class PolozkaForm(ModelForm):
    class Meta:
        model = Polozka
        fields = ['wine', 'mnozstvi']
        widgets = {
            'cena_polozky': HiddenInput(),  # Skryté pole pro cenu položky
        }

class CommentForm(ModelForm):
    class Meta:
        model = Comment
        fields = '__all__'