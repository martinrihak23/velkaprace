from django.contrib import admin
from .models import Wine,Objednavka, Employee, Customer,Comment,Polozka
# Register your models here.

admin.site.register(Wine)
admin.site.register(Objednavka)
admin.site.register(Employee)
admin.site.register(Customer)
admin.site.register(Comment)
admin.site.register(Polozka)