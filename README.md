# Název projektu

## Popis projektu

Krátký popis projektu a jeho účelu.

### Instalace

1. Naklonujte repozitář:

    ```bash
    git clone https://gitlab.com/vase-jmeno/nazev-repozitare.git
    ```

2. Vstupte do adresáře:

    ```bash
    cd nazev-repozitare
    ```

3. Vytvořte a aktivujte virtuální prostředí:

    ```bash
    python -m venv env
    source env/bin/activate  # Na Windows použijte env\Scripts\activate
    ```

4. Nainstalujte závislosti:

    ```bash
    pip install -r requirements.txt
    ```

5. Spusťte aplikaci:

    ```bash
    python manage.py runserver
    ```

## Předpoklady

- Python 3.x
- Django

## Jak přispívat

1. Forkněte repozitář.
2. Vytvořte větev pro vaše změny:

    ```bash
    git checkout -b nazev-vetve
    ```

3. Proveďte změny a commitujte je:

    ```bash
    git commit -m "Popis změn"
    ```

4. Pushněte změny do své větve:

    ```bash
    git push origin nazev-vetve
    ```

5. Otevřete pull request na GitLabu.

## Licence

Projekt je licencován pod licencí MIT - více informací najdete v souboru LICENSE.
